
import java.util.Scanner;

public class BankingMain1 {

	public static void main(String[] args) {

		BankCalculation cust1 = new BankCalculation();

		double depositAmount = 0;
		cust1.deposit(depositAmount);
		double withdrawalAmount = 0;
		cust1.withdrawal(withdrawalAmount);

		System.out.println("Welcome to the Ceylon Banking System");

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter Your Name:");

		String name = scan.nextLine();

		int action = 0;

		do {
			System.out.print("Enter  current balance:");
			double balance = scan.nextDouble();
			while (balance < 0) {
				System.out.println("***Beginning balance must be at least zero,please re-enter");
				System.out.print("Enter  current balance:");
				balance = scan.nextDouble();
				if (balance > 0) {
					break;
				}
				balance++;
			}
			cust1.Accountbalance = balance;

			int numberofdeposits = 0;
			int numberofwithdrawals = 0;

			do {
				System.out.print("Enter the number of deposits(0-5):");
				numberofdeposits = scan.nextInt();

				if (0 > numberofdeposits || 5 < numberofdeposits) {
					System.out.println("***Invalid number of deposits,please re-enter.Enter the number of deposits:");
				}
			} while (0 > numberofdeposits || 5 < numberofdeposits);

			do {
				System.out.print("Enter the number of  withdrawals(0-5):");
				numberofwithdrawals = scan.nextInt();

				if (0 > numberofwithdrawals || 5 < numberofwithdrawals) {
					System.out.println(
							"***Invalid number of  numberofwithdrawals,please re-enter.Enter the number of  numberofwithdrawals:");
				}
			} while (0 > numberofwithdrawals || 5 < numberofwithdrawals);

			double currentdepositamount = 0;
			for (int i = 1; i <= numberofdeposits; i++) {
				do {
					System.out.print("Enter the amount of deposit #" + i + ":");
					depositAmount = scan.nextDouble();

					if (depositAmount < 0) {
						System.out.print("***deposit amount must be grater than zero,please re-enter");
						depositAmount = scan.nextDouble();
					}
					currentdepositamount = currentdepositamount + depositAmount;
				} while (depositAmount < 0);
			}

			double currentwithdrawalamount = 0;

			for (int j = 1; j <= numberofwithdrawals; j++) {
				do {
					System.out.print("Enter the amount of withdrawal #" + j + ":");
					withdrawalAmount = scan.nextDouble();

					if (withdrawalAmount > balance) {
						System.out.print("***withdrawal amount exceeds current balance,please re-enter");
						withdrawalAmount = scan.nextDouble();

					}
					currentwithdrawalamount = currentwithdrawalamount + withdrawalAmount;
				} while (withdrawalAmount < 0);
			}

			double closingbalance = ((balance + currentdepositamount) - currentwithdrawalamount);
			System.out.println(" *** the closing balance  is Rs." + closingbalance + " ***");

			if (closingbalance >= 50000.00) {
				System.out.println("***" + name + "  " + "it is time to invest some money.***");
			} else {
				if (closingbalance >= 15000.00 && closingbalance <= 49999.99) {
					System.out.println("***" + name + "  " + "you should consider the CD.***");
				} else {
					if (closingbalance >= 1000.00 && closingbalance <= 14999.99) {
						System.out.println("***" + name + "  " + "keep up the good work.***");
					} else {
						if (closingbalance >= 0.00 && closingbalance <= 999.99) {
							System.out.println("***" + name + "  " + "your balance is getting low.***");
						}
					}

				}
			}

			System.out.println("Do you continue...  YES = press 1  or  NO = press 0");
			action = scan.nextInt();

			if (action == 0) {
				System.out.println("Thank you!!!");
				System.exit(0);
			}
		} while (action == 1);
	}
}
