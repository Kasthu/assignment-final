package Part2;

import java.util.Scanner;

/*prime number is a positive integer which is divisible only by 1 and itself for example:2,3,5,7,11 */
public class Prime {
	public static void main(String args[]) {
		// c-count
		int num, i, c;
		Scanner scan = new Scanner(System.in);
		// Enter the numberToCheck we want to check for prime
		System.out.println("Enter A Number");
		// it scans the next token as an int value
		num = scan.nextInt();
		i = 1;
		c = 0;

		while (i <= num) {
			// numberToCheck is dived by itself
			if ((num % i) == 0)
				c = c + 1;
			i = i + 1;
		}
		if (c == 2)
			System.out.println(num + " is a prime number");
		else
			System.out.println(num + " is not a prime number");
	}
}
